const chatBox = document.getElementById('chat-box');
const messageInput = document.getElementById('message-input');
const sendButton = document.getElementById('send-button');

sendButton.addEventListener('click', () => {
    const message = messageInput.value;
    if (message.trim() === '') return;

    const sender = 'You';
    const timestamp = new Date().toLocaleTimeString();
    const newMessageElement = document.createElement('div');
    newMessageElement.classList.add('message');

    const senderElement = document.createElement('span');
    senderElement.classList.add('sender');
    senderElement.textContent = `${sender} - ${timestamp}`;
    newMessageElement.appendChild(senderElement);

    const messageContent = document.createElement('p');
    messageContent.textContent = message;
    newMessageElement.appendChild(messageContent);

    chatBox.appendChild(newMessageElement);

    messageInput.value = '';
});
